import asyncio
import httpx
import os

DISCORD_TOKEN = os.environ.get("DISCORD_TOKEN")
SPEED = 0.75 # changes per second

if not DISCORD_TOKEN:
    raise RuntimeError("There is no DISCORD_TOKEN in current environment!")

async def change_status(new_status: dict, token: str) -> str:
    async with httpx.AsyncClient(http2=True) as client:
        headers = {
            "content-type": "application/json",
            "authorization": token
        }
        data = {
            "custom_status": new_status
        }
        response = await client.patch('https://discord.com/api/v8/users/@me/settings', headers=headers, json=data)
        if response.status_code != httpx.codes.OK:
            raise RuntimeError("Failed to change status!")
        return response.json()["custom_status"]

async def get_status(token: str) -> str:
    async with httpx.AsyncClient(http2=True) as client:
        headers = {
            "content-type": "application/json",
            "authorization": token
        }
        response = await client.get('https://discord.com/api/v8/users/@me/settings', headers=headers)
        if response.status_code != httpx.codes.OK:
            raise RuntimeError("Failed to get status!")
        return response.json()["custom_status"]

async def main():
    remainder = 1000

    while 7:        
        diff = remainder - 7

        new_status = {
            'text': f'{remainder} - 7 = {diff}',
            'expires_at': None,
            'emoji_id': None,
            'emoji_name': None
        }

        remainder = diff
        if remainder < 0:
            remainder = 1000
        status_task = asyncio.create_task(change_status(new_status, DISCORD_TOKEN))                
        await asyncio.sleep(1 / SPEED)

if __name__ == "__main__":    
    old_status = asyncio.run(get_status(DISCORD_TOKEN))
    print(f"Current status: {old_status}")
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("Returning previous status...")
        asyncio.run(change_status(old_status, DISCORD_TOKEN))        
