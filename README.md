# i am a ghoul
## cool 1000 - 7 subtracter in discord status for real dead insides
>\+ it saves your current status before running and returns it on exit

## Requirements
*for modern async version*
* python3 >= 3.7
* httpx

## Installation
1. Clone repo or download zip
2. Resolve requirements
3. fill DISCORD_TOKEN environment variable with your discord token
4. Done

## Run
```bash
$ python3 src/dynstatus.py
```
